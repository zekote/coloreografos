package modelo;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Scanner;

public class Loader {

	public static Grafo cargar(String rutaArcEnt){
		Scanner in;
		try {
			in = new Scanner(new FileReader(rutaArcEnt));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		Grafo migrafo = new Grafo(0, 0);
		
		String lineaActual;
		
		while (in.hasNext()) {
			lineaActual = in.nextLine();
			
			switch (lineaActual.charAt(0)) {
			case 'c':
				System.out.println(lineaActual.substring(2));
				break;
			case 'p':
				String[] campos = lineaActual.split(" ");
				migrafo.setCantAristas(Integer.parseInt(campos[3]));
				migrafo.setCantNodos(Integer.parseInt(campos[2]));
				break;
			case 'e':
				String[] campos2 = lineaActual.split(" ");
				migrafo.agregarArista(Integer.parseInt(campos2[1]), Integer.parseInt(campos2[2]));
				break;
			}
		}
		in.close();
		return migrafo;
	}
}
