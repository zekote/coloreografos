package modelo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;

public class Grafo {

	private ArrayList<Nodo> listaNodos;
	public ArrayList<Nodo> getListaNodos() {
		return listaNodos;
	}

	private HashMap<Integer, Nodo> indiceNodos;
	private int aristas;
	private int nodos;
	private int maxGrado;
	
	
	public int getMaxGrado() {
		return maxGrado;
	}

	public Grafo(int aristas, int nodos) {
		super();
		this.aristas = aristas;
		this.nodos = nodos;	
		this.listaNodos = new ArrayList<Nodo>();
		this.indiceNodos = new HashMap<Integer, Nodo>();
		this.maxGrado = 0;
		
	}
	
	public void agregarArista(int uno, int dos){
		Nodo unnodo = indiceNodos.get(uno);
		Nodo otronodo = indiceNodos.get(dos);
		
		if (unnodo == null) {
			unnodo = new Nodo(uno);
			this.indiceNodos.put(uno, unnodo);
			this.listaNodos.add(unnodo);
		}
		if (otronodo == null){
			otronodo = new Nodo(dos);
			this.indiceNodos.put(dos, otronodo);
			this.listaNodos.add(otronodo);
		}
		
		unnodo.agregarvecino(otronodo);
		otronodo.agregarvecino(unnodo);
		
		if (unnodo.getGradoActual() > this.maxGrado){
			this.maxGrado = unnodo.getGradoActual();
		}
		if (otronodo.getGradoActual() > this.maxGrado){
			this.maxGrado = otronodo.getGradoActual();
		}
		
	}
	
	public void imprimir(){
		
		for (Iterator<Nodo> iterator = this.listaNodos.iterator(); iterator.hasNext();) {
			Nodo unnodo = iterator.next();
			System.out.print(unnodo.getId()+" Color: "+unnodo.getColorActual()+"->(");
			unnodo.imprimirVecinos();
			System.out.println(")");
		}
		
	}
	
	public void imprimirColor(){
		for (Iterator<Nodo> iterator = this.listaNodos.iterator(); iterator.hasNext();) {
			Nodo unnodo = iterator.next();
			System.out.print(unnodo.getColorActual()+" ");
		}
	}

	public int getAristas() {
		return aristas;
	}

	public int getNodos() {
		return nodos;
	}

	public void setCantNodos(int cant_nodos) {
		this.nodos = cant_nodos;
		
	}

	public void setCantAristas(int cant_aristas) {
		this.aristas = cant_aristas;
		
	}
	
	public void ordenarMayorMenor(){
		Collections.sort(this.listaNodos);
	}
	
	public void ordenarMenorMayor(){
		Collections.reverse(this.listaNodos);
	}
}
