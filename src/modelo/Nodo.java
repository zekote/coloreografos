package modelo;

import java.util.ArrayList;
import java.util.Iterator;

public class Nodo implements Comparable<Nodo> {

	private int colorActual;
	private int grado;
	public ArrayList<Nodo> listaVecinos;
	private int id;
	
	
	public Nodo(int identificador, int colorActual) {
		super();
		this.colorActual = colorActual;
		this.grado = 0;
		this.listaVecinos = new ArrayList<Nodo>();
		this.id = identificador;
	}

	public Nodo(int identificador) {
		super();
		this.colorActual = 9999;
		this.grado = 0;
		this.listaVecinos = new ArrayList<Nodo>();
		this.id = identificador;
	}
	

	@Override
	public String toString() {
		return "" + id + "";
	}

	public int compareTo(Nodo otronodo) {

		return otronodo.getGradoActual() - this.getGradoActual();

	}

	@Override
	public boolean equals(Object obj) {
		
		return ((Nodo)obj).getId() == this.getId();
	}

	public int getColorActual() {
		return colorActual;
	}


	public void setColorActual(int colorActual) {
		this.colorActual = colorActual;
	}


	public int getGradoActual() {
		return grado;
	}


	public void aumentaUnGrado() {
		this.grado = this.grado + 1;
	}
	
	public void disminuyeUnGrado() {
		this.grado = this.grado - 1;
	}


	public ArrayList<Nodo> getListaVecinos() {
		return listaVecinos;
	}


	public void agregarvecino (Nodo unnodo) {
		if (!this.listaVecinos.contains(unnodo)){
		this.listaVecinos.add(unnodo);
		this.aumentaUnGrado();
		}
	}

	public int getId() {
		return id;
	}
	
	public void imprimirVecinos(){
		for (Iterator<Nodo> iterator = this.listaVecinos.iterator(); iterator.hasNext();) {
			Nodo unnodo = iterator.next();
			System.out.print(unnodo.getId()+ ",");
		}
	}
	
}
