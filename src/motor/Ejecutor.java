package motor;


import estrategias.HeuristicaHorizonteCercano;
import estrategias.HeuristicaHorizonteL2;
import modelo.Grafo;
import modelo.Loader;

public class Ejecutor {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		String rutaArch=args[0];
		heuristica1(rutaArch);
		heuristica2(rutaArch);
		
	}
	
	public static void heuristica1(String rut){
		System.out.println("Heuristica Golosa");
		System.out.println("---------Comentarios Archivo -------------");
		String ruta = rut;
		long cargainicio = System.currentTimeMillis();
		Grafo migrafo = Loader.cargar(ruta);
		long cargafin = System.currentTimeMillis();
		long cargaresult = cargafin - cargainicio; 
		HeuristicaHorizonteCercano heu = new HeuristicaHorizonteCercano(migrafo);
//		HeuristicaHorizonteL2 heu = new HeuristicaHorizonteL2(migrafo);
		long ejecucioninicio = System.currentTimeMillis();
		heu.ejecutar();
		long ejecucionfin = System.currentTimeMillis();
		long ejecucionresult = ejecucionfin - ejecucioninicio;
		System.out.println("---------Detalle del grafo----------");
		System.out.println("Grado del Grafo: "+migrafo.getMaxGrado());
		System.out.println("Numero de Nodos: "+migrafo.getNodos());
		System.out.println("Numero de aristas: "+migrafo.getAristas());
		System.out.println("---------Detalle de Corrida----------");
		System.out.println("solucion valida hallada: "+heu.solucionValida());
		System.out.println("Tiempo de carga en ms: "+cargaresult);
		System.out.println("Tiempo de ejecucion en ms: "+ejecucionresult);
		System.out.println("Funcional de la solución encontrada: " +heu.getSolucion());
		migrafo.imprimirColor();
		
	}
	
	public static void heuristica2 (String rut){
		System.out.println("");
		System.out.println("");
		System.out.println("");
		System.out.println("Heuristica Segundo orden");

		System.out.println("---------Comentarios Archivo -------------");
		String ruta = rut;
		long cargainicio = System.currentTimeMillis();
		Grafo migrafo = Loader.cargar(ruta);
		long cargafin = System.currentTimeMillis();
		long cargaresult = cargafin - cargainicio; 
//		HeuristicaHorizonteCercano heu = new HeuristicaHorizonteCercano(migrafo);
		HeuristicaHorizonteL2 heu = new HeuristicaHorizonteL2(migrafo);
		long ejecucioninicio = System.currentTimeMillis();
//		heu.ejecutar();
		heu.ejecutar();
		long ejecucionfin = System.currentTimeMillis();
		long ejecucionresult = ejecucionfin - ejecucioninicio;
		System.out.println("---------Detalle del grafo----------");
		System.out.println("Grado del Grafo: "+migrafo.getMaxGrado());
		System.out.println("Numero de Nodos: "+migrafo.getNodos());
		System.out.println("Numero de aristas: "+migrafo.getAristas());
		System.out.println("---------Detalle de Corrida----------");
		System.out.println("solucion valida hallada: "+heu.solucionValida());
		System.out.println("Tiempo de carga en ms: "+cargaresult);
		System.out.println("Tiempo de ejecucion en ms: "+ejecucionresult);
		System.out.println("Funcional de la solución encontrada: " +heu.getSolucion());
		migrafo.imprimirColor();
		System.out.println("");
	}

}
