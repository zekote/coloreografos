package estrategias;

import java.util.ArrayList;
import java.util.Iterator;
import modelo.Grafo;
import modelo.Nodo;

public class HeuristicaHorizonteCercano implements Heuristica {

	private Grafo migrafo;
	private int poolcolores;
	private int solucion;
	
	public int getSolucion() {
		return solucion + 1;
	}

	public HeuristicaHorizonteCercano(Grafo migrafo) {
		super();
		this.migrafo = migrafo;
		this.poolcolores = migrafo.getMaxGrado();
		this.solucion = 0;
	}

	@Override
	public void ejecutar() {
		this.migrafo.ordenarMayorMenor();
		
		int aux;
		
		int vueltas = 1;
		while (vueltas > 0){
			for (Iterator<Nodo> iterator = this.migrafo.getListaNodos().iterator(); iterator.hasNext();) {
				Nodo unnodo = iterator.next();
				aux = this.disminuirEnergia(unnodo);
				if (aux > this.solucion){
					this.solucion = aux;
				}
			}
			vueltas --;
			if (vueltas == 0 & !this.solucionValida()){
				vueltas = 2;
			}
			this.migrafo.ordenarMenorMayor();
		}
	}
	
	private int disminuirEnergia(Nodo unnodo){
		
		ArrayList<Integer> coloresAdyacentes = new ArrayList<Integer>();
		
		//busco los colores adyacentes
		for (Iterator<Nodo> iterator = unnodo.listaVecinos.iterator(); iterator.hasNext();) {
			Nodo vecino = iterator.next();
			coloresAdyacentes.add(vecino.getColorActual());
		}
		
		int colorcandidato = 0;
		while (true){
			
			if (colorcandidato > this.poolcolores){
				this.poolcolores ++;
			}
			
			if (!coloresAdyacentes.contains(colorcandidato)){
				unnodo.setColorActual(colorcandidato);
				return colorcandidato;
			}
			colorcandidato++;
		}
	}
	
	public boolean solucionValida(){
		
		for (Iterator<Nodo> iterator = this.migrafo.getListaNodos().iterator(); iterator.hasNext();) {
			Nodo unnodo = iterator.next();
			for (Iterator<Nodo> iterator2 = unnodo.listaVecinos.iterator(); iterator2.hasNext();) {
				Nodo vecino = iterator2.next();
//				System.out.println("IDs: "+unnodo.getId()+" "+vecino.getId()+" Colores:"+unnodo.getColorActual()+vecino.getColorActual());
				if (unnodo.getColorActual() == vecino.getColorActual()){
					System.out.println("id1: "+unnodo.getId()+" Color: "+unnodo.getColorActual());
					System.out.println("id2: "+vecino.getId()+" Color: "+vecino.getColorActual());
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public boolean esValido(Nodo unnodo) {
		// TODO Auto-generated method stub
		return false;
	}

	public int getNivelEnergia(Nodo unnodo){
		return 0;
	}

	@Override
	public void setGrafo(Grafo ungrafo) {
		this.migrafo = ungrafo;
		
	}

}
