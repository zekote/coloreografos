package estrategias;

import modelo.Grafo;
import modelo.Nodo;

public interface Heuristica {

	void ejecutar();
	
	boolean esValido(Nodo unnodo);
	
	void setGrafo(Grafo ungrafo);
	
}
