package estrategias;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import modelo.Grafo;
import modelo.Nodo;

public class HeuristicaHorizonteL2 implements Heuristica {

	private Grafo migrafo;
	private int poolcolores;
	private int solucion;
	
	public int getSolucion() {
		HashSet<Integer> lista = new HashSet<Integer>();
		
		for (Nodo unnodo : this.migrafo.getListaNodos()) {
			lista.add(unnodo.getColorActual());
		}
		return lista.size();
	}

	public HeuristicaHorizonteL2(Grafo migrafo) {
		super();
		this.migrafo = migrafo;
		this.poolcolores = migrafo.getMaxGrado();
		this.solucion = 0;
	}

	@Override
	public void ejecutar() {
		this.migrafo.ordenarMayorMenor();
		
		int aux;
		
		int vueltas = 10;
		while (vueltas > 0){
			for (Iterator<Nodo> iterator = this.migrafo.getListaNodos().iterator(); iterator.hasNext();) {
				Nodo unnodo = iterator.next();
				aux = this.disminuirEnergia(unnodo);
				if (aux > this.solucion){
					this.solucion = aux;
				}
			}
			vueltas --;
			if (vueltas == 0 & !this.solucionValida()){
				vueltas = 2;
			}
			//this.migrafo.ordenarMenorMayor();
		}
	}
	
	private int disminuirEnergia(Nodo unnodo){
		
		ArrayList<Integer> coloresAdyacentes = new ArrayList<Integer>();
		ArrayList<ColorAparicion> colores2nivel = new ArrayList<ColorAparicion>();
		HashSet<Nodo> listaVecinosSegnivel = new HashSet<Nodo>();
		
		//ArrayList<Nodo> listaVecinosSegnivel = new ArrayList<Nodo>();
		
		//busco los colores adyacentes y cargo una lista con potenciales vecinos de segundo nivel
		for (Iterator<Nodo> iterator = unnodo.listaVecinos.iterator(); iterator.hasNext();) {
			Nodo vecino = iterator.next();
			
			if (!coloresAdyacentes.contains(vecino.getColorActual())){
				coloresAdyacentes.add(vecino.getColorActual());
			}
			listaVecinosSegnivel.addAll(vecino.listaVecinos);
		}
		
		//Como los vecinos de mis vecinos pueden ser mis vecinos ( a-b  | a-c-b) limpio la lista
		
		listaVecinosSegnivel.removeAll(unnodo.listaVecinos);
		listaVecinosSegnivel.remove(unnodo); // yo soy vecino de mis vecinos, asi que me remuevo de la lista.
		
		
		//busco los colores de los vecinos de segundo nivel
		
		for (Nodo nodo : listaVecinosSegnivel) {
			
			ColorAparicion aux = ColorAparicion.getpar(nodo.getColorActual(), 1);
			if (!colores2nivel.contains(aux)){
				colores2nivel.add(aux);
			}else{
				colores2nivel.get(colores2nivel.indexOf(aux)).aumentarApariciones();
			}
		}
		
		Collections.sort(colores2nivel);
	
		int colorcandidato = 0;
		
		
		for (ColorAparicion color : colores2nivel) {
			if (!coloresAdyacentes.contains(color.color) && color.getColor()!=9999){
				unnodo.setColorActual(color.getColor());
				return color.getColor();
			}
		}
		
		while (true){
			
			if (colorcandidato > this.poolcolores){
				this.poolcolores ++;
			}
			
			if (!coloresAdyacentes.contains(colorcandidato)){
				unnodo.setColorActual(colorcandidato);
				return colorcandidato;
			}
			colorcandidato++;
		}
	}

	
	public boolean solucionValida(){
		
		for (Iterator<Nodo> iterator = this.migrafo.getListaNodos().iterator(); iterator.hasNext();) {
			Nodo unnodo = iterator.next();
			for (Iterator<Nodo> iterator2 = unnodo.listaVecinos.iterator(); iterator2.hasNext();) {
				Nodo vecino = iterator2.next();
//				System.out.println("IDs: "+unnodo.getId()+" "+vecino.getId()+" Colores:"+unnodo.getColorActual()+vecino.getColorActual());
				if (unnodo.getColorActual() == vecino.getColorActual()){
					System.out.println("id1: "+unnodo.getId()+" Color: "+unnodo.getColorActual());
					System.out.println("id2: "+vecino.getId()+" Color: "+vecino.getColorActual());
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public boolean esValido(Nodo unnodo) {
		// TODO Auto-generated method stub
		return false;
	}

	public int getNivelEnergia(Nodo unnodo){
		return 0;
	}

	@Override
	public void setGrafo(Grafo ungrafo) {
		this.migrafo = ungrafo;
		
	}

}
