package estrategias;

public class ColorAparicion implements Comparable<ColorAparicion> {

	int color;
	int apariciones;
	public ColorAparicion(int color, int apariciones) {
		super();
		this.color = color;
		this.apariciones = apariciones;
	}
	public int getColor() {
		return color;
	}
	public void setColor(int color) {
		this.color = color;
	}
	public int getApariciones() {
		return apariciones;
	}
	@Override
	public String toString() {
		String result = this.color +" "+this.apariciones;
		return result;
	}
	public void setApariciones(int apariciones) {
		this.apariciones = apariciones;
	}
	
	public static ColorAparicion getpar(int color, int apariciones){
		return new ColorAparicion(color, apariciones);
	}
	
	public int compareTo(ColorAparicion otro) {

		return otro.getApariciones() - this.getApariciones();

	}

	@Override
	public boolean equals(Object obj) {
		
		return ((ColorAparicion)obj).getColor() == this.getColor();
	}
	
	public void aumentarApariciones(){
		this.apariciones++;
	}

	
}
